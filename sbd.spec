%global longcommit cf5c2208bad2db2dff9b09624b89b05415c3bc11
%global shortcommit %(echo %{longcommit}|cut -c1-8)
%global modified %(echo %{longcommit}-|cut -f2 -d-)
%global github_owner Clusterlabs
%global buildnum 3

%global watchdog_timeout_default 5

# Be careful with sync_resource_startup_default
# being enabled. This configuration has
# to be in sync with configuration in pacemaker
# where it is called sbd_sync - assure by e.g.
# mutual rpm dependencies.
%bcond_without sync_resource_startup_default
# Syncing enabled per default will lead to
# syncing enabled on upgrade without adaption
# of the config.
# Setting can still be overruled via sysconfig.
# The setting in the config-template packaged
# will follow the default if below is is left
# empty. But it is possible to have the setting
# in the config-template deviate from the default
# by setting below to an explicit 'yes' or 'no'.
%global sync_resource_startup_sysconfig ""

Name:           sbd
Summary:        Storage-based death
License:        GPL-2.0-or-later
Group:          System Environment/Daemons
Version:        1.5.2
Release:        %{buildnum}
Url:            https://github.com/%{github_owner}/%{name}
Source0:        https://github.com/%{github_owner}/%{name}/archive/%{longcommit}/%{name}-%{longcommit}.tar.gz
Patch0:         0001-Fix-the-problem-of-service-error-when-uninstalling.patch
Patch1:         0001-Fix-query-watchdog-avoid-issues-on-heap-allocation-f.patch
Patch2:         0002-Refactor-sbd-md-alloc-de-alloc-reverse-order.patch
Patch3:         0003-spec-convert-license-naming-to-SPDX.patch
Patch4:         0004-test-load-libaio.so-instead-of-libaio.so.1.patch
Patch5:         backport-Fix-sbd-inquisitor-avoid-parsing-SBD_DELAY_START-as-.patch
Patch6:         backport-Refactor-tests-ability-to-specify-a-libaio-name.patch
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libuuid-devel
BuildRequires:  glib2-devel
BuildRequires:  libaio-devel
BuildRequires:  corosynclib-devel
BuildRequires:  pacemaker-libs-devel > 1.1.12
BuildRequires:  libtool
BuildRequires:  libuuid-devel
BuildRequires:  libxml2-devel
BuildRequires:  pkgconfig
BuildRequires:  systemd
BuildRequires:  make
Conflicts:      fence-agents-sbd < 4.5.0
Conflicts:      pacemaker-libs < 2.1.0

ExclusiveArch: x86_64 aarch64 riscv64 loongarch64 ppc64le
%if %{defined systemd_requires}
%systemd_requires
%endif

%description

This package contains the storage-based death functionality.

Available rpmbuild rebuild options:
  --with(out) : sync_resource_startup_default

%package        tests
Summary:        Storage-based death environment for regression tests
License:        GPL-2.0-or-later

%description tests
This package provides an environment + testscripts for
regression-testing sbd.

%prep
%autosetup -p1 -n %{name}-%{longcommit}

%build
./autogen.sh
export CFLAGS="$RPM_OPT_FLAGS -Wall -Werror"
%configure --with-watchdog-timeout-default=%{watchdog_timeout_default} \
           --with-sync-resource-startup-default=%{?with_sync_resource_startup_default:yes}%{!?with_sync_resource_startup_default:no} \
           --with-sync-resource-startup-sysconfig=%{sync_resource_startup_sysconfig} \
           --with-runstatedir=%{_rundir}
make %{?_smp_mflags}

%install

make DESTDIR=$RPM_BUILD_ROOT LIBDIR=%{_libdir} install
rm -rf ${RPM_BUILD_ROOT}%{_libdir}/stonith

install -D -m 0755 tests/regressions.sh $RPM_BUILD_ROOT/usr/share/sbd/regressions.sh
%if %{defined _unitdir}
install -D -m 0644 src/sbd.service $RPM_BUILD_ROOT/%{_unitdir}/sbd.service
install -D -m 0644 src/sbd_remote.service $RPM_BUILD_ROOT/%{_unitdir}/sbd_remote.service
%endif

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
install -m 644 src/sbd.sysconfig ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/sbd

# Don't package static libs
find %{buildroot} -name '*.a' -type f -print0 | xargs -0 rm -f
find %{buildroot} -name '*.la' -type f -print0 | xargs -0 rm -f

%clean
rm -rf %{buildroot}


%if %{defined _unitdir}
%post
%systemd_post sbd.service
%systemd_post sbd_remote.service
if [ $1 -ne 1 ] ; then
	if systemctl --quiet is-enabled sbd.service 2>/dev/null
	then
		systemctl --quiet reenable sbd.service 2>/dev/null || :
	fi
	if systemctl --quiet is-enabled sbd_remote.service 2>/dev/null
	then
		systemctl --quiet reenable sbd_remote.service 2>/dev/null || :
	fi
fi

%preun
%systemd_preun sbd.service
%systemd_preun sbd_remote.service

%postun
%systemd_postun sbd.service
%systemd_postun sbd_remote.service
%endif

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sysconfig/sbd
%{_sbindir}/sbd
%{_datadir}/sbd
%{_datadir}/pkgconfig/sbd.pc
%exclude %{_datadir}/sbd/regressions.sh
%doc %{_mandir}/man8/sbd*
%if %{defined _unitdir}
%{_unitdir}/sbd.service
%{_unitdir}/sbd_remote.service
%endif
%doc COPYING

%files tests
%defattr(-,root,root)
%dir %{_datadir}/sbd
%{_datadir}/sbd/regressions.sh
%{_libdir}/libsbdtestbed*

%changelog
* Thu Dec 05 2024 liupei <liupei@kylinos.cn> - 1.5.2-3
- Fix: sbd-inquisitor: avoid parsing SBD_DELAY_START as a time duration if its value is boolean false
- Refactor: tests: ability to specify a libaio name

* Thu Apr 25 2024 zouzhimin <zouzhimin@kylinos.cn> - 1.5.2-2
- test: load libaio.so instead of libaio.so.1

* Wed Mar 27 2024 bizhiyuan <bizhiyuan@kylinos.cn> - 1.5.2-1
- Update to 1.5.2

* Wed Mar 6 2024 xurui <rui.xu@shingroup.cn> - 1.5.1-4
- Add ppc64le to ExclusiveArch

* Fri Nov 24 2023 xuxiaojuan <xuxiaojuan@kylinos.cn> - 1.5.1-3
- Add loongarch64 to ExclusiveArch

* Mon Jun 05 2023 laokz <zhangkai@iscas.ac.cn> - 1.5.1-2
- Add riscv64 to ExclusiveArch

* Thu Feb 02 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.5.1-1
- Update package to version 1.5.1

* Mon Mar 28 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.4.0-17
- Fix the problem of service error when uninstalling

* Thu Dec 16 2021 liqiuyu <liqiuyu@kylinos.cn> - 1.4.0-16
- Remove the release suffix

* Fri Oct 30 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 1.4.0-15
- Init sbd project
